import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [],
})

export class AppComponent {
  private questionsUrl = './../assets/questions.json';
  private questions = [];
  private questionIndex = 0;
  private isSubmitView: Boolean = false;
  private isAllAnswered: Boolean = undefined;
  private score = 0;
  constructor(private http: Http) {
    let obj;
    this.getJSON().subscribe(data => obj = data, error => console.log(error));
  }
  submitAnswers() {
    if (this.checkAllAnswered()) {
      for (const answer of this.questions) {
        if (answer.selected === answer.correct_answer) {
          this.score += 10;
        }
      }
      this.isSubmitView = true;
    } else {
      this.isAllAnswered = false;
    }
  }

  checkAllAnswered() {
    let allAnswered = false;
    for (const answer of this.questions) {
      if (answer.selected === '') {
        return allAnswered;
      }
    }
    allAnswered = true;
    return allAnswered;
  }

  retryQuiz() {
    this.score = 0;
    this.questionIndex = 0;
    this.isAllAnswered = undefined;
    for (const answer of this.questions) {
      answer.selected = '';
    }
    this.isSubmitView = false;
  }

  checkQues(ques, answer) {
    this.questions[this.questionIndex].selected = answer;
    console.log(this.questions);
  }
  decrement() {
    this.questionIndex--;
  }
  increment() {
    this.questionIndex++;
  }
  /**
   * Fetch JSON file from assets.
   */
  public getJSON(): Observable<any> {
    return this.http.get(this.questionsUrl)
      .map((res: any) => {
        this.questions = this.shuffleArray(res.json().questions);
        for (const question of this.questions) {
          question.answers = this.shuffleArray(question.answers);
        }
        console.log(this.questions);
      })
      .catch((error: any, caught: Observable<any>) => {
        console.log(error);
        return Observable.throw(caught);
      });
  }

  /**
   * shuffle answers and quetions
   * Using Durstenfeld shuffle algorithm.
   */
  public shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor((i + 1) * Math.random());
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

}
